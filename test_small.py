from small_server import SmallFactory
from twisted.trial import unittest
from twisted.test import proto_helpers


class SmallFactoryTestCase(unittest.TestCase):
    def setUp(self):
        self.instring = "hi there"
        self.factory = SmallFactory()
        self.proto = self.factory.buildProtocol(('127.0.0.1', 0))

    def test_small(self):
        self.tr = proto_helpers.StringTransport()
        self.assertFalse(self.proto.connectionmade)
        self.proto.makeConnection(self.tr)
        self.assertTrue(self.proto.connectionmade)
        self.proto.dataReceived(self.instring)
        outstring = self.tr.value()
        self.assertEqual(outstring, self.instring)
        self.assertTrue(self.proto.connectionlost)

    def test_small_with_disconnection(self):
        self.tr = proto_helpers.StringTransportWithDisconnection()
        self.assertFalse(self.proto.connectionmade)
        self.proto.makeConnection(self.tr)
        self.assertTrue(self.proto.connectionmade)
        self.proto.dataReceived(self.instring)
        outstring = self.tr.value()
        self.assertEqual(outstring, self.instring)
        self.assertTrue(self.proto.connectionlost)
