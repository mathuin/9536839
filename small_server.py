from twisted.internet.protocol import Factory, Protocol


class SmallFactory(Factory):
    def buildProtocol(self, addr):
        return Small(self)


class Small(Protocol):
    def __init__(self, factory):
        self.factory = factory
        self.connectionmade = False
        self.connectionlost = False

    def connectionMade(self):
        self.connectionmade = True

    def dataReceived(self, data):
        self.output = data
        self.transport.write(self.output)
        self.transport.loseConnection()

    def connectionLost(self):
        self.connectionlost = True
